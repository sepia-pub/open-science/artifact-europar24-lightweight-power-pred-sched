#!/usr/bin/env python3
import argparse
import datetime
import json
import os
import numpy as np
import pandas as pd

def main():
    datetime_parser = lambda f: datetime.datetime.strptime(f, '%Y-%m-%d %H:%M:%S')
    parser = argparse.ArgumentParser()
    parser.add_argument("input_jobs_gantt", help='path to the Batsim jobs output CSV file')
    parser.add_argument("input_batsim_workload", help='path to the Batsim workload JSON file')
    parser.add_argument("input_workload_root_path", help="filepath to the location of the root directory of the generated workloads")
    parser.add_argument("--end", type=int, default=10800, help='the end (in seconds) until when the computation should be done')
    parser.add_argument("--powercap_watts", required=True, type=float, help='the value in watts of the dynamic powercap')
    parser.add_argument("-o", "--output_prefix", required=True, help="filepath prefix where all output files should be generated")
    args = parser.parse_args()
    assert args.end >= 0

    with open(args.input_batsim_workload) as f:
        batsim_workload = json.load(f)
    batsim_out_jobs = pd.read_csv(args.input_jobs_gantt)

    # determine which jobs are in the computation window
    jobs_in_window_mask = batsim_out_jobs['starting_time'] < args.end
    jobs_in_window = batsim_out_jobs[jobs_in_window_mask].copy().reset_index()

    window_nb_values = args.end

    # precompute stuff to retrieve jobs info
    job_data = {}
    job_ids = set([str(x) for x in jobs_in_window['job_id']])
    for job_info in batsim_workload['jobs']:
        if job_info['id'] in job_ids:
            job_data[job_info['id']] = {
                'job_details_filepath': job_info['extra_data']['job_details_filepath']
            }

    # initialize power values by the static power
    platform_power_values_during_window = np.zeros(window_nb_values)
    utilization_during_window = np.zeros(window_nb_values)
    assert window_nb_values == len(platform_power_values_during_window)

    # compute the power consumed of all jobs in [0, end].
    # - use input data when the job is scheduled
    # - complete with zeros
    for _, job_row in jobs_in_window.iterrows():
        job_id = str(job_row['job_id'])
        job_details_filepath = job_data[job_id]['job_details_filepath']

        job_dynpower_ts = pd.read_csv(f'{args.input_workload_root_path}/{job_details_filepath}/dynpower.csv')

        start_time_index = int(job_row['starting_time'])
        end_time_index = max(0, min(int(job_row['finish_time']) - 1, window_nb_values))

        # add prefix zeros
        nb_prefix_zeros = int(max(0, start_time_index - 1))
        job_power_values = np.zeros(nb_prefix_zeros)
        job_utilization = np.zeros(nb_prefix_zeros)
        nb_added_values = nb_prefix_zeros

        # add watts from the M100 trace
        for value in job_dynpower_ts['job_total_dynamic_power']:
            nb_values_to_add = min(20, window_nb_values - nb_added_values)
            job_power_values = np.concatenate([job_power_values, np.ones(nb_values_to_add) * value])
            job_utilization = np.concatenate([job_utilization, np.ones(nb_values_to_add) * job_row['requested_number_of_resources']])
            nb_added_values += nb_values_to_add
            if nb_added_values == window_nb_values:
                break

        # add suffix zeros
        nb_suffix_zeros = window_nb_values - nb_added_values
        job_power_values = np.concatenate([job_power_values, np.zeros(nb_suffix_zeros)])
        job_utilization = np.concatenate([job_utilization, np.zeros(nb_suffix_zeros)])
        assert window_nb_values == len(job_power_values)
        assert window_nb_values == len(job_utilization)

        platform_power_values_during_window += job_power_values
        utilization_during_window += job_utilization
        assert window_nb_values == len(platform_power_values_during_window)
        assert window_nb_values == len(utilization_during_window)

    df = pd.DataFrame({
        'time': [int(x) for x in range(window_nb_values)],
        'power': platform_power_values_during_window,
    })
    df.to_csv(f'{args.output_prefix}sim-dynpower.csv', index=False)

    # compute metrics from the power consumption over time
    integral_from_powercap = platform_power_values_during_window - args.powercap_watts
    above_powercap = integral_from_powercap[integral_from_powercap > 0]
    under_powercap = integral_from_powercap[integral_from_powercap < 0]

    quantiles_power = np.quantile(platform_power_values_during_window, [0.01, 0.1, 0.5, 0.9, 0.99])
    energy_from_powercap = integral_from_powercap.sum()
    surplus_energy = above_powercap.sum()
    unused_energy = -under_powercap.sum()

    mean_power = platform_power_values_during_window.mean()
    max_power_from_powercap = integral_from_powercap.max()
    min_power_from_powercap = integral_from_powercap.min()
    nb_seconds_above_powercap = len(above_powercap)

    # compute job metrics
    mean_waiting_time = batsim_out_jobs['waiting_time'].mean()
    mean_turnaround_time = batsim_out_jobs['turnaround_time'].mean()
    mean_slowdown = batsim_out_jobs['stretch'].mean()

    # other infrastructure metrics
    mean_utilization = utilization_during_window.mean()
    max_utilization = utilization_during_window.max()

    # print all metrics
    metrics = {
        'energy_from_powercap': energy_from_powercap,
        'surplus_energy': surplus_energy,
        'unused_energy': unused_energy,

        'power_p1': quantiles_power[0],
        'power_p10': quantiles_power[1],
        'power_p50': quantiles_power[2],
        'power_p90': quantiles_power[3],
        'power_p99': quantiles_power[4],

        'mean_power': mean_power,
        'max_power_from_powercap': max_power_from_powercap,
        'min_power_from_powercap': min_power_from_powercap,
        'nb_seconds_above_powercap': nb_seconds_above_powercap,

        'mean_waiting_time': mean_waiting_time,
        'mean_turnaround_time': mean_turnaround_time,
        'mean_slowdown': mean_slowdown,

        'mean_utilization': mean_utilization,
        'max_utilization': max_utilization,
    }

    print(json.dumps(metrics, sort_keys=True, allow_nan=False))
    with open(f'{args.output_prefix}sim-metrics.csv', 'w') as f:
        f.write(json.dumps(metrics, sort_keys=True, allow_nan=False, indent=2))
