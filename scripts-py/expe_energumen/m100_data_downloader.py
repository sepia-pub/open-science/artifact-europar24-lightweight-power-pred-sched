'''
Download a M100 trace month and extract power/job parquet files from it
'''
#!/usr/bin/env python3
import argparse
import os
import re
import subprocess

# month_id, url_format(filename: "{month_id}.tar"), md5sum
files_to_download = {
    '22-01': ('https://zenodo.org/record/7589942/files/{filename}?download=1', '127ce355ea476f3e9b9faeea3a7d1fbe'),
    '22-02': ('https://zenodo.org/record/7589942/files/{filename}?download=1', '04fad708ac2e51492541364ad90daad0'),
    '22-03': ('https://zenodo.org/record/7590061/files/{filename}?download=1', '4c74ad95b6d837d409e24216389bb25e'),
    '22-04': ('https://zenodo.org/record/7590308/files/{filename}?download=1', '1ddf1c851a728b1bb002e6fd9a8e265e'),
    '22-05': ('https://zenodo.org/record/7590547/files/{filename}?download=1', '54d6c1de33aa934bee6215f4a4a2ddb6'),
    '22-06': ('https://zenodo.org/record/7590555/files/{filename}?download=1', '171b6abddc7325ea2804fc8273845f9b'),
    '22-07': ('https://zenodo.org/record/7590565/files/{filename}?download=1', '5511880e8a755ba7bf68a34e5a3a65ac'),
    '22-08': ('https://zenodo.org/record/7590574/files/{filename}?download=1', '169366482a17790f49894632492da905'),
    '22-09': ('https://zenodo.org/record/7590583/files/{filename}?download=1', '9caa1b51b37bd8800ea7f5977c609467'),
}

def retrieve_files_from_month(month_id, url_format, expected_md5, output_dir, tmp_dir='/tmp',
    dry_run=False,
    do_download=True, do_check_md5=True, do_extract=True, do_copies=True, do_clear_extracted_dir=True, do_clear_downloaded_file=True,
):
    downloaded_filename = f'{month_id}.tar'
    downloaded_filepath = f'{tmp_dir}/{downloaded_filename}'
    url = url_format.format(filename=downloaded_filename)
    if do_download:
        args = ['curl', '-L', '-o', downloaded_filepath, url]
        if dry_run:
            args.insert(0, 'echo')
        subprocess.run(args, check=True)

    if do_check_md5:
        args = ['md5sum', downloaded_filepath]

        if not dry_run:
            p = subprocess.run(args, check=True, capture_output=True, encoding='utf-8')
            r = re.compile('''^([0-9A-Fa-f]{32})\s+.*$''')
            m = r.match(p.stdout)
            md5_computed_value = m.group(1)
            if md5_computed_value != expected_md5:
                raise RuntimeError(f"md5 checksum mismatch for month={month_id}. expected_md5={expected_md5}. got={md5_computed_value}. url={url}")
        else:
            args.insert(0, 'echo')
            subprocess.run(args, check=True)

    if do_extract:
        if not dry_run:
            os.makedirs(tmp_dir, exist_ok=True)

        args = ['tar', '-xf', downloaded_filepath, f'--directory={tmp_dir}']
        if dry_run:
            args.insert(0, 'echo')
        subprocess.run(args, check=True)

    extracted_root = f'{tmp_dir}/year_month={month_id}'
    if do_copies:
        if not dry_run:
            os.makedirs(output_dir, exist_ok=True)

        args_list = list()
        args_list.append(['cp', f'{extracted_root}/plugin=job_table/metric=job_info_marconi100/a_0.parquet',
                                f'{output_dir}/{month_id}_jobs.parquet'])
        args_list.append(['cp', f'{extracted_root}/plugin=ipmi_pub/metric=total_power/a_0.parquet',
                                f'{output_dir}/{month_id}_power_total.parquet'])

        if dry_run:
            for args in args_list:
                args.insert(0, 'echo')

        for args in args_list:
            subprocess.run(args, check=True)

    if do_clear_extracted_dir:
        args = ['rm', '-rf', extracted_root]
        if dry_run:
            args.insert(0, 'echo')

        subprocess.run(args, check=True)

    if do_clear_downloaded_file:
        args = ['rm', downloaded_filepath]
        if dry_run:
            args.insert(0, 'echo')

        subprocess.run(args, check=True)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("output_dir", help="the output directory. example value: '/home/alice/out'")
    parser.add_argument("month", nargs='+', help="the month to download. example value: '22-07'")
    parser.add_argument("--tmp-dir", default='/tmp', help="the temporary directory. default value: '/tmp'")
    parser.add_argument("--no-download", action='store_true', help='do not download the archive from Zenodo')
    parser.add_argument("--no-md5", action='store_true', help='do not md5 checksum the downloaded archive')
    parser.add_argument("--no-extract", action='store_true', help='do not extract the archive')
    parser.add_argument("--no-copy", action='store_true', help='do not copy files from the extracted archive to the output dir')
    parser.add_argument("--no-rm-ext-dir", action='store_true', help='do not remove the extracted directory')
    parser.add_argument("--no-rm-archive", action='store_true', help='do not remove the archive')
    parser.add_argument("--dry-run", action='store_true', help="do not do anything but show what the script would do without this option")
    args = parser.parse_args()

    for month in args.month:
        if month not in files_to_download:
            raise RuntimeError(f"unknown month '{month}'")

        download_info = files_to_download[month]
        retrieve_files_from_month(month, *files_to_download[month], args.output_dir,
            tmp_dir=args.tmp_dir, dry_run=args.dry_run,
            do_download=not args.no_download,
            do_check_md5=not args.no_md5,
            do_extract=not args.no_extract,
            do_copies=not args.no_copy,
            do_clear_extracted_dir=not args.no_rm_ext_dir,
            do_clear_downloaded_file=not args.no_rm_archive,
        )

if __name__ == '__main__':
    main()
